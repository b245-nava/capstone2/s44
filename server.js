const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

// Routes
const userRoutes = require('./Routes/userRoutes.js');
const prodRoutes = require('./Routes/prodRoutes.js');
const orderRoutes = require('./Routes/orderRoutes.js');


const port = 4000;
const app = express();


// Mongo Connection + Error-Handling

	mongoose.set('strictQuery', true);
	mongoose.connect('mongodb+srv://admin:admin@batch245-nava.y30w9yr.mongodb.net/Capstone2_Nava?retryWrites=true&w=majority', {
		useNewUrlParser: true,
		useUnifiedTopology: true
	});


	let db = mongoose.connection;

	db.on('error', console.error.bind(console, 'Connection Error.'));
	db.once('open', () => {console.log('Cloud database connection successful.')});

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());


// Routing
app.use('/user', userRoutes);
app.use('/product', prodRoutes);
app.use('/order', orderRoutes);


app.listen(port, () => console.log(`Server is live at port ${port}.`));