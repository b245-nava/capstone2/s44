const express = require('express');
const router = express.Router();
const orderController = require('../Controllers/orderControllers.js');
const cartController = require('../Controllers/cartControllers.js');
const auth = require('../auth.js');

// [Order Routes]

// Create Order
router.post('/checkout', auth.verify, orderController.checkout);

/*// [Cart Routes]

// Starting new cart by adding a product
router.post('/startCart/:prodId', auth.verify, cartController.newCart);

// Adding a specified product to the cart
router.post('/addToCart/:cartId/:prodId', auth.verify, cartController.addToCart);*/

module.exports = router;