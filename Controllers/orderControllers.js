const mongoose = require('mongoose');
const Order = require('../Models/orderSchema.js');
const Product = require('../Models/prodSchema.js');
const User = require('../Models/userSchema.js');
const auth = require('../auth.js');

// Creating new order
/*
	Business Logic:
		1. Only users w/o admin access can create orders.
		2. An order must contain the ff:
			- userId of registered customer making order
			- list of ordered products (array of objects: indicate product ID and quantity of item)
			- user's address
			- total of order

		This means that:
			- UserId must be pushed onto order's records, while the order's ID can be associated with the user that made it
			- The chosen product's ID needs to be pushed onto the order form, and quantity needs to be indicated
			- The user needs to indicate the address to which their order will be delivered.

	Possible user-end errors:
		1. User is an admin
		2. Invalid product ID

	Possible code bugs:
		1. Non-existent product ID gets added into order anyway
		2. totalAmount does not automatically and precisely reflect the total price of the order
*/

module.exports.checkout = (request, response) => {

	const userData = auth.decode(request.headers.authorization); // to verify if user is admin and to extract userId
	const input = request.body; // product IDs, quantity, and address go here (follow orderSchema)

	if(userData.isAdmin){
		return response.send('Please log into your user account to make purchases!');
	} else {

		let newOrder = new Order({
			userId: userData._id,
			noodle: input.noodle,
			topping1: input.topping1,
			topping2: input.topping2,
			topping3: input.topping3,
			sauce: input.sauce,
			address: input.address,
			quantity: input.quantity,
			instructions: input.instructions
		});

		let totalAmount = newOrder.totalAmount;

		// Verifying noodle ID and price
		Product.findById(input.noodle)
		.then(noodle => {

			if(noodle === null){
				return response.send('Incorrect noodle ID. Please double-check and try again!')
			} else {

				let noodlePrice = noodle.price;

				totalAmount = totalAmount + noodlePrice;

				// Verifying topping 1's ID and price
				Product.findById(input.topping1)
				.then(topping => {

					if(topping === null){
						return response.send('Incorrect topping ID. Please double-check and try again!')
					} else {

						let toppingPrice = topping.price;

						totalAmount = totalAmount + toppingPrice;

						// Verifying topping 2
						Product.findById(input.topping2)
						.then(topping2 => {

							if(topping2 === null){
								return response.send('Incorrect topping ID. If you did not want a second topping, please enter the code for No Toppings!');
							} else {

								totalAmount = totalAmount + topping2.price;

								// Verifying topping 3
								Product.findById(input.topping3)
								.then(topping3 => {

									if(topping3 === null){
										return response.send('Incorrect topping ID. If you did not want a second topping, please enter the code for No Toppings!');
									} else {

										// If user wants 3 toppings and has selected a regular size wok, the size will automatically be upgraded to large.
										if(topping.price>0 && topping2.price>0 && topping3.price>0 && noodlePrice === 120){

											Product.findById('63e1a18dff5e2f4aec490d51')
											.then(large => {

												if(!large.isAvailable){
													return response.send('You have selected too many toppings. Please select none for the third topping to proceed.')
												} else {

													totalAmount = totalAmount + topping3.price + (large.price - 120);

													// Verifying sauce ID and price
													Product.findById(input.sauce)
													.then(sauce => {

														if(sauce === null){
															return response.send('Incorrect sauce ID. Please double-check and try again!')
														} else {

															newOrder.totalAmount = totalAmount + sauce.price;

															newOrder.totalAmount = newOrder.totalAmount*input.quantity;

															// All product IDs and prices should have been verified by this point
															newOrder.save()
															.then(result => {
																console.log(result);
																return response.send(`Thank you for placing your order with us! Your receipt number is ${result._id} and your total is ${result.totalAmount}. (Please be informed that your wok size has been upgraded to Large since you have selected three toppings, hence the additional charge of 40.)`)
																// Assuming the code flow is correct, result.totalAmount should reflect the total of the noodles, toppings, and sauce.
															})
															.catch(err => {
																console.log(err);
																return response.send('Sorry, there was an error in saving your order. Please try again!')
															});
														}
													})
													.catch(err => {
														console.log(err);
														return response.send('Sorry, there was an error. Please try again!');
													})
												}
											})
											.catch(err => {
												console.log(err);
												return response.send('Sorry, there was an error. Please try again!');
											})

										} else {

											totalAmount = totalAmount + topping3.price;

											// Verifying sauce ID and price
											Product.findById(input.sauce)
											.then(sauce => {

												if(sauce === null){
													return response.send('Incorrect sauce ID. Please double-check and try again!')
												} else {

													newOrder.totalAmount = totalAmount + sauce.price;

													newOrder.totalAmount = newOrder.totalAmount*input.quantity;

													// All product IDs and prices should have been verified by this point
													newOrder.save()
													.then(result => {
														console.log(result);
														return response.send(`Thank you for placing your order with us! Your receipt number is ${result._id} and your total is ${result.totalAmount}.`)
														// Assuming the code flow is correct, result.totalAmount should reflect the total of the noodles, toppings, and sauce.
													})
													.catch(err => {
														console.log(err);
														return response.send('Sorry, there was an error in saving your order. Please try again!')
													});
												}
											})
											.catch(err => {
												console.log(err);
												return response.send('Sorry, there was an error. Please try again!');
											})
										}
									}
								})
								.catch(err => {
									console.log(err);
									return response.send('Sorry, there was an error. Please try again!');
								})
							}
						})
						.catch(err => {
							console.log(err);
							return response.send('Sorry, there was an error. Please try again!');
						})
					}
				})
				.catch(err => {
					console.log(err);
					return response.send('Sorry, there was an error. Please try again!');
				})
			}
		})
		.catch(err => {
			console.log(err);
			return response.send('Sorry, there was an error. Please try again!');
		});
	}
};