const mongoose = require('mongoose');
const Cart = require('../Models/cartSchema.js');
const User = require('../Models/userSchema.js');
const Product = require('../Models/prodSchema.js')
const auth = require('../auth.js');

// Cart
/*
	Business Logic:
		- What is a "cart" for? It's for:
			- Holding and storing the user's chosen products
			- The user can reach in and add products to the cart, or remove products as they please
		- Since admins can't check out products, they shouldn't be able to use a cart either
		- A cart is automatically created for users when they select products to buy, and a user can have multiple carts
		- The cart is "returned" once the user checks out their products at the cashier
*/

// Controller for starting a cart
module.exports.newCart = (request, response) => {

	const userData = auth.decode(request.headers.authorization);
	const prodId = request.params.prodId;
	const input = request.body;

	if(userData.isAdmin){
		return response.send('Please log into your user account to make purchases!')
	} else {

		User.findById({_id: userData._id})
		.then(result => {

			let userInfo = result;

			Product.findById({_id: prodId})
			.then(result => {

				if(result === null){
					return response.send('Please double-check the product ID and try again.')
				} else {

					let newCart = new Cart({
						userId: userData._id,
						products: [
							{
								productId: result._id,
								quantity: input.quantity
							}
						],
						total: result.price*input.quantity
					});

					newCart.save()
					.then(result => {

						/*console.log(userInfo);
						userInfo.cart.push({cartId: result._id});
						Will not push cart ID to user's "cart" property for some reason*/
						console.log(result);
						return response.send(`Your item has been added to your cart. Here is your cart ID: ${result._id}`)
					})
					.catch(err => {
						console.log(err);
						return response.send('Sorry, there was an error. Please try again.')
					});
				}
			})
		})
		.catch(err => {
			console.log(err);
			return response.send('User verification error. Please try again!')
		})
	}
};

// Adding to cart
/*module.exports.addToCart = (request, response) => {

	const userData = auth.decode(request.headers.authorization);
	const cartId = request.params.cartId;
	const prodId = request.params.prodId;

	if(userData.isAdmin){
		return response.send('Please log into your user account to make purchases!')
	} else {

		
	}
}*/