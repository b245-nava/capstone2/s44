const mongoose = require('mongoose');
const Product = require('../Models/prodSchema.js');
const auth = require('../auth.js');

// Product Creation
module.exports.prodCreate = (request, response) => {

	const userData = auth.decode(request.headers.authorization);
	const input = request.body;

	if(!userData.isAdmin){
		return response.send('Sorry, you are not authorized to do this. Please contact your administrator if you think this is a mistake.');
	} else {

		Product.findOne({name: input.name})
		.then(result => {

			if(result !== null){
				return response.send('This product already exists. Did you mean to update your product?');
			} else {

				let newProd = new Product({
					name: input.name,
					description: input.description,
					price: input.price,
					class: input.class
				})

				newProd.save()
				.then(result => {
					return response.send(`Your product ${newProd.name} has been created!`)
				})
				.catch(err => {
					console.log(err)
					return response.send(`Sorry, there was an error. Please try again.`)
				})
			}
		})
		.catch(err => response.send(err))
	}
};

// View Menu (Only available products)
module.exports.menu = (request, response) => {
	Product.find({isAvailable: true})
	.then(result => response.send(result))
	.catch(err => {
		console.log(err);
		return response.send('Sorry, there was an error. Please try again!')
	})
};

// View Menu (Available Wok)
module.exports.menuWok = (request, response) => {
	Product.find({class: "wok", isAvailable: true})
	.then(result => response.send(result))
	.catch(err => {
		console.log(err);
		return response.send('Sorry, there was an error. Please try again!')
	})
};

// View Menu (Available Toppings)
module.exports.menuTops = (request, response) => {
	Product.find({class: "toppings", isAvailable: true})
	.then(result => response.send(result))
	.catch(err => {
		console.log(err);
		return response.send('Sorry, there was an error. Please try again!')
	})
};

// View Menu (Available Sauces)
module.exports.menuSauce = (request, response) => {
	Product.find({class: "sauce", isAvailable: true})
	.then(result => response.send(result))
	.catch(err => {
		console.log(err);
		return response.send('Sorry, there was an error. Please try again!')
	})
};

// Retrieve a single product
module.exports.menuOne = (request, response) => {

	const menuId = request.params.menuId;

	Product.findById(menuId)
	.then(result => response.send(result))
	.catch(err => response.send(err))

};

// Edit a specified product
module.exports.prodEdit = (request, response) => {

	const userData = auth.decode(request.headers.authorization);
	const menuId = request.params.menuId;
	const input = request.body;

	if(!userData.isAdmin){
		return response.send('Sorry, you are not authorized to do this. Please contact your administrator if you think this is a mistake.')
	} else {

		Product.findById({_id: menuId})
		.then(result => {

			if(result === null){
				return response.send('Could not find the product. Please double-check the ID and try again!')
			} else {

				let menuUpdate = {
					name: input.name,
					description: input.description,
					price: input.price,
					class: input.class
				};

				Product.findByIdAndUpdate(menuId, menuUpdate, {new: true})
				.then(result => response.send(result))
				.catch(err => {
					console.log(err);
					return response.send('Sorry, there was an error. Please try again.')
				})
			}
		})
		.catch(err => {
			console.log(err);
			return response.send('Sorry, there was an error. Please try again.')
		});
	}
};

// Archive a specified product
module.exports.prodArchive = (request, response) => {

	const userData = auth.decode(request.headers.authorization);
	const menuId = request.params.menuId;
	const input = request.body;

	if(!userData.isAdmin){
		return response.send('Sorry, you are not authorized to do this. Please contact your administrator if you think this is a mistake.');
	} else {

		Product.findById({_id: menuId})
		.then(result => {

			if(result === null){
				return response.send('Product not found. Please double-check the ID and try again.');
			} else {

				let updatedStatus = {
					isAvailable: input.isAvailable
				};

				Product.findByIdAndUpdate(menuId, updatedStatus, {new: true})
				.then(result => response.send(result))
				.catch(err => {
					console.log(err);
					return response.send('Sorry, there was an error. Please try again.')
				});
			}
		})
		.catch(err => {
			console.log(err);
			return response.send('Sorry, there was an error. Please try again.')
		});
	}
};