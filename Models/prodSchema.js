const mongoose = require('mongoose');

const prodSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, 'Please name your product.']
	},
	description: {
		type: String,
		required: [true, 'Please describe your product.']
	},
	price: {
		type: Number,
		required: [true, 'Please enter a price.']
	},
	class: {
		type: String,
		required: [true, 'Please specify product type.']
	},
/*	variations: [{
		sauce: {
			type: String,
			default: "None"
		},
		toppings: {
			type: String,
			default: "None"
		},
		noodles: {
			type: String,
			default: "Rice Noodles"
		}
	}], will just include sauces (all free), toppings (check site for the prices), and noodle type (udon is 1.50 euros) among products*/
	isAvailable: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	}
});

module.exports = mongoose.model("Product", prodSchema);