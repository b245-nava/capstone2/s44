const mongoose = require('mongoose');

const cartSchema = new mongoose.Schema({
	userId: {
		type: String,
		required: [true, "Please specify user associated to cart."]
	},
	products: [{
		productId: {
			type: String,
			required: [true, "Please specify the product you want."]
		},
		quantity: {
			type: Number,
			default: 1
		}
	}],
	total: {
		type: Number
	}
});



module.exports = mongoose.model("Cart", cartSchema);