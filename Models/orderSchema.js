const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({
	userId: {
		type: String,
		required: [true, "Please include purchasing user details."]
	},
	noodle: {
		type: String,
		required: [true, "Please indicate the noodles you want."]
	},
	topping1: {
		type: String,
		required: [true, "Please select either one topping or indicate none."]
	},
	topping2: {
		type: String,
		default: "none"
	},
	topping3: {
		type: String,
		default: "none"
	},
	sauce: {
		type: String,
		default: "none"
	},
	address: {
		type: String,
		required: [true, "Please indicate where you would like the delivery to go."]
	},
	quantity: {
		type: Number,
		default: 1
	},
	instructions: {
		type: String,
		default: "none"
	},
	totalAmount: {
		type: Number,
		default: 0
	},
	purchasedOn: {
		type: Date,
		default: new Date()
	}
});

module.exports = mongoose.model("Order", orderSchema);